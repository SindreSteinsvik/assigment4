

// Getting the HTML elements
const computerFeatures = document.getElementById("computerFeatures")
const computerTitle = document.getElementById("computerTitle");
const computerDesc = document.getElementById("computerDesc");
const computerPrice = document.getElementById("computerPrice");
const computerSelectEl = document.getElementById("computers");
const computerSpecs = document.getElementById("Features");
const computerImage = document.getElementById("computerImg");




const btnGetALoan = document.getElementById("btnGetALoan");
const btnWork = document.getElementById("btnWork")
const btnBank = document.getElementById("btnBank")
const pay = document.getElementById("txtWork");
const btnBuyNow = document.getElementById("btnBuyNow");
const btnPayLoan = document.getElementById("btnPayLoan");
const loansButtonsEL = document.getElementById("loansButtons");
const outstandingLoanEL = document.getElementById("outstandingLoan");
const balanceEl = document.getElementById("txtBalance");
const currLoanEl = document.getElementById("txtLoan");
const GET_PAYED = parseInt(100);

// variables
let currLoan = 0;
let currPay = 0;
let currBalance = 0;
let isLoan = false;


//All the different eventListeners
//Gets 100 in pay after clicking work button
btnWork.addEventListener("click", function() {
currPay = addMoney(currPay, GET_PAYED)
setInnerText(pay, currPay);
})

//Prompt the user and checks if the user can take a loan.  
btnGetALoan.addEventListener("click", function () {
    let tempLoan = prompt("Take out a loan");
    loan = Number(tempLoan);
    if (loan !== null && loan != 0) {
        if (loan <= currBalance * 2 && isLoan == false) {
          currBalance = addMoney(currBalance, loan);
          currLoan = addMoney(currLoan, loan);
    
          isLoan = true;
          disableButton(btnGetALoan);
          hideElement(btnGetALoan);
          showElement(btnPayLoan);
          setInnerText(balanceEl, currBalance);
          showLoan(currLoan);
        }
      }
    
});

// Transfers the money from pay to balance. if user has a loan, 10% pays down the loan
btnBank.addEventListener("click", function () {
    let tenPercent = 0;
    if (pay.innerText != 0) {
      if (currLoan > 0) {
        tenPercent = currPay * 0.1;
        currLoan = reduceMoney(currLoan, tenPercent);
        if (currLoan < 0) currLoan = 0;
      }
  
      currBalance = addMoney(currBalance, currPay - tenPercent)
      currPay = 0;
  
      setInnerText(pay, currPay);
      setInnerText(balanceEl, currBalance);
  
      if (currLoan > 0) setInnerText(currLoanEl, ` -${currLoan}`);
      else {
        hideLoanElements();
        enableButton(btnGetALoan);
      }
    } else alert("You have no money to transfer.");
});

  // Transfers money from pay to loan, when you have a loan
  btnPayLoan.addEventListener("click", function () {
    if (currPay >= currLoan) {
      currPay = reduceMoney(currPay, currLoan);
      currLoan = 0;
      currBalance = addMoney(currBalance, currPay);
      currPay = 0;
  
      isLoan = false;
  
      setInnerText(balanceEl, currBalance);
      setInnerText(pay, currPay);
  
      showElement(btnGetALoan);
      enableButton(btnGetALoan);
      hideLoanElements();
    }
  });

  // Checks if you have enough money in your balance to buy a computer
  btnBuyNow.addEventListener("click", function () {
    const tempPrice = Number(computerPrice.innerText);
    if (currBalance >= tempPrice) {
      currBalance = reduceMoney(currBalance, tempPrice);
      setInnerText(balanceEl, currBalance);
      alert(
        `GZ! You bought the ${computerTitle.innerText}`
      );
    } else alert("You can't afford this computer! You have to work.");
  });
  
  //Event listener for the select tag. calls the onSelectChange when a computer is selected
  computerSelectEl.addEventListener("change", onSelectChange)


// Fetching the computers from the API
  const computers = []
  fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then((response) => {
      return response.json()
  })
  
  .then((jsonComputers) => {
      console.log(jsonComputers)
      computers.push(...jsonComputers)
      renderComputers(jsonComputers)
  })
  
  //This function loops through the array of computers and add the computers in the select tag
  function renderComputers(computers){
      for (const computer of computers){
          computerSelectEl.innerHTML += `<option value=${computer.id}>${computer.title}</option>`
      }
  }

  //Connects the data fetched from the Api to the HTML tags
  function renderComputer(selectedComputer){
    computerFeatures.innerText= `Features: `;
    computerImage.src = "https://noroff-komputer-store-api.herokuapp.com/" + selectedComputer.image;
    computerPrice.innerText = selectedComputer.price;
    computerDesc.innerText = selectedComputer.description;
    computerTitle.innerText = selectedComputer.title 
  //Loops through the array of specs 
    computerSpecs.innerHTML= ""
    for (const key in selectedComputer.specs){
        const spec = selectedComputer.specs[key]
        computerSpecs.innerHTML += `<li>${spec}</li>`
    }


  }
  //This method is called when selecting a computer in the select tag.  the buy now button is shown, the computer selected is found by is and the renderComputer method is called
  function onSelectChange(){
      showElement(btnBuyNow)
      const computerId = this.value
      const selectedComputer = computers.find(g => g.id == computerId)
      renderComputer(selectedComputer)
  }


  
  



//Help Functions
function reduceMoney(target, amount){
    return target-amount;
}

function addMoney(target, amount){
    return target+amount;
}

function showLoan(currLoan) {
    showElement(outstandingLoanEL.childNodes[1]);
    showElement(outstandingLoanEL.childNodes[3]);
  
    currLoanEl.innerText = `- ${currLoan}`;
}
  
function hideLoan() {
    hideElement(outstandingLoanEL.childNodes[1]);
    hideElement(outstandingLoanEL.childNodes[3]);
}
  
function showElement(element) {
    element.classList.remove("hidden");
}
  
function hideElement(element) {
    element.classList.add("hidden");
}
  
function enableButton(button) {
    button.disabled = false;
}
  
function disableButton(button) {
    button.disabled = true;
}
  
function setInnerText(element, text) {
    element.innerText = text;
}
  
function hideLoanElements() {
    hideLoan();
    hideElement(btnPayLoan);
}
  
  